export default {
  reporters: ["default", "jest-junit"],
  collectCoverage: true,
  coverageDirectory: "coverage",
  collectCoverageFrom: ["**/*.{js,jsx}", "!**/node_modules/**", "!coverage/**"],
};
